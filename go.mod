module gitlab.com/EternalWanderer/sheet-parser/v2

go 1.19

require (
	github.com/fatih/color v1.13.0
	gitlab.com/EternalWanderer/dice-roller v0.0.0-20220820120015-fa11a906e0aa
)

require (
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)
