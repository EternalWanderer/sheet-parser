package main

type Character struct {
	Misc   Misc    `json:"misc"`
	Stats  []Stat  `json:"stats"`
	Skills []Skill `json:"skills"`
	Spells []Spell `json:"spells"`
	Feats  []Feat  `json:"feats"`
}

type Misc struct {
	Level            int    `json:"level"`
	Name             string `json:"name"`
	Race             string `json:"race"`
	Class            string `json:"class"`
	Background       string `json:"background"`
	PersonalityTrait string `json:"personalityTrait"`
	Ideals           string `json:"ideals"`
	Bonds            string `json:"bonds"`
	Flaws            string `json:"flaws"`
	Quirk            string `json:"quirk"`

	ReliableTalent  bool `json:"reliableTalent"`
	JackOfAllTrades bool `json:"jackOfAllTrades"`
	IsKurthog       bool `json:"isKurthog"`
}

type Skill struct {
	Name       string `json:"name"`
	Proficient bool   `json:"proficient"`
	Expertise  bool   `json:"expertise"`
	BaseStat   string `json:"baseStat"`
}

type Stat struct {
	Name       string `json:"name"`
	Score      int    `json:"score"`
	Proficient bool   `json:"proficient"`
}

type Spell struct {
	Name          string `json:"name"`
	Level         int    `json:"level"`
	CastingTime   string `json:"castingTime"`
	Range         int    `json:"range"`
	Components    string `json:"components"`
	Duration      string `json:"duration"`
	Attack_Save   string `json:"attack_save"`
	Damage_Effect string `json:"damage_effect"`
	Description   string `json:"description"`
	School        string `json:"school"`
	AtHighLevel   string `json:"atHighLevel"`
}

type Feat struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}
