VERSION = 0.6
PREFIX = /usr/local
MANPAGE = sheet-parser.1
MANPREFIX = $(PREFIX)/share/man
ZSH_COMPLETION_OUTPUT := zsh.completion

SRC = main.go go.mod sheetContent.go go.sum

all: sheet-parser

sheet-parser:
	go build -o sheet-parser -trimpath

clean: 
	rm -f sheet-parser sheet-parser-$(VERSION).tar.gz $(MANPAGE)

dist: clean
	mkdir -p sheet-parser-$(VERSION)
	cp -R example.json $(ZSH_COMPLETION_OUTPUT) LICENSE Makefile README.md $(MANPAGE).scd $(SRC) sheet-parser-$(VERSION)
	tar -cf sheet-parser-$(VERSION).tar sheet-parser-$(VERSION)
	gzip sheet-parser-$(VERSION).tar
	rm -rf sheet-parser-$(VERSION)

install: all $(MANPAGE)
	install -dm 0755 $(DESTDIR)$(PREFIX)/bin
	install -m 0755 sheet-parser $(DESTDIR)$(PREFIX)/bin
	install -dm 0755 $(DESTDIR)$(MANPREFIX)/man1
	install -m 0644 $(MANPAGE) $(DESTDIR)$(MANPREFIX)/man1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/sheet-parser\
		$(DESTDIR)$(MANPREFIX)/man1/$(MANPAGE)

package: dist
	rsync --progress sheet-parser-$(VERSION).tar.gz voidDroplet:/var/www/alpine/src/
	rm -f /var/cache/distfiles/sheet-parser*
	abuild checksum
	abuild -r

$(MANPAGE): $(MANPAGE).scd
	scdoc < $< > $@

.PHONY: all sheet-parser clean dist install uninstall package
